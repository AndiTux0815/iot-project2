#!/bin/bash

INTERVAL=20
HTMLOUTPUTFILE="/opt/iotproject/index.html"
HTMLTEMPLATE="/opt/iotproject/etc/index.html.template"

mosquitto_sub -h localhost -t '#' -F "%t %p" | while read toppic payload
 do
  if [ "$topic" = "sensor/temp" ]
  then
  zeit=$(date +%H:%M:%S)
  cat HTMLTEMPLATE | sed s/_ZEIT_/$zeit/g | sed s/_temp_/$payload/g > HTMLOUTPUTFILE
 fi
done
