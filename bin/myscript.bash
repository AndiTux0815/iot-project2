#!/bin/bash

INTERVAL=20
HTMLOUTPUTFILE="/opt/iotproject/index.html"
HTMLTEMPLATE="/opt/iotproject/etc/index.html.template"

mosquitto_sub -h localhost -t '#' -F "%t %p" | while read topic payload ; do
 if [ "$topic" = "Temperatur" ]
 then
     zeit=$(date +%H:%M:%S)
     echo $zeit
     cat $HTMLTEMPLATE | sed s/_ZEIT_/$zeit/g | sed s/_TEMP_/$payload/g > $HTMLOUTPUTFILE
 fi
done
